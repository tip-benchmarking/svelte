function Send-MultiPartForm {
    # Attribution: [@akauppi's post](https://stackoverflow.com/a/25083745/419956)
    # Remixed in: [@jeroen's post](https://stackoverflow.com/a/41343705/419956)
    [CmdletBinding(SupportsShouldProcess = $true)] 
    param (
        [Parameter(Position = 0)]
        [string]
        $Uri,

        [Parameter(Position = 1)]
        [HashTable]
        $FormEntries,

        [Parameter(Position = 2, Mandatory = $false)]
        [System.Management.Automation.Credential()]
        [System.Management.Automation.PSCredential]
        $Credential,

        [Parameter(
            ParameterSetName = "FilePath",
            Mandatory = $true,
            ValueFromPipeline = $true,
            ValueFromPipelineByPropertyName = $true
        )]
        [Alias("Path")]
        [string[]]
        $FilePath,

        [Parameter()]
        [string]
        $FilesKey = "files"
    );

    begin {
        $LF = "`n"
        $boundary = [System.Guid]::NewGuid().ToString()

        Write-Verbose "Setting up body with boundary $boundary"

        $bodyArray = @()

        foreach ($key in $FormEntries.Keys) {
            $bodyArray += "--$boundary"
            $bodyArray += "Content-Disposition: form-data; name=`"$key`""
            $bodyArray += ""
            $bodyArray += $FormEntries.Item($key)
        }

        Write-Verbose "------ Composed multipart form (excl files) -----"
        Write-Verbose ""
        foreach($x in $bodyArray) { Write-Verbose "> $x"; }
        Write-Verbose ""
        Write-Verbose "------ ------------------------------------ -----"

        $i = 0
    }

    process {
        $fileName = (Split-Path -Path $FilePath -Leaf)

        Write-Verbose "Processing $fileName"

        $fileBytes = [IO.File]::ReadAllBytes($FilePath)
        $fileDataAsString = ([System.Text.Encoding]::GetEncoding("iso-8859-1")).GetString($fileBytes)

        $bodyArray += "--$boundary"
        $bodyArray += "Content-Disposition: form-data; name=`"$FilesKey[$i]`"; filename=`"$fileName`""
        $bodyArray += "Content-Type: application/x-msdownload"
        $bodyArray += ""
        $bodyArray += $fileDataAsString

        $i += 1
    }

    end {
        Write-Verbose "Finalizing and invoking rest method after adding $i file(s)."

        if ($i -eq 0) { throw "No files were provided from pipeline." }

        $bodyArray += "--$boundary--"

        $bodyLines = $bodyArray -join $LF

        # $bodyLines | Out-File data.txt # Uncomment for extra debugging...

        try {
            if (!$WhatIfPreference) {
                Invoke-RestMethod `
                    -Uri $Uri `
                    -Method Post `
                    -ContentType "multipart/form-data; boundary=`"$boundary`"" `
                    -Credential $Credential `
                    -Body $bodyLines
            } else {
                Write-Host "WHAT IF: Would've posted to $Uri body of length " + $bodyLines.Length
            }
        } catch [Exception] {
            throw $_ # Terminate CmdLet on this situation.
        }

        Write-Verbose "Finished!"
    }
}

# Based on https://stackoverflow.com/a/10585857
function GetFiles($path = $pwd, [string[]]$exclude){
    foreach ($item in Get-ChildItem $path){
        if (
            (
                $exclude | Where {$item -like $_}
            ) -bor (
                (
                    $item.Attributes -band
                    [System.IO.FileAttributes]::Hidden
                ) -eq [System.IO.FileAttributes]::Hidden
            )
        ) {
            continue
        }

        if (Test-Path $item.FullName -PathType Container){
            GetFiles $item.FullName $exclude # Recursively continue
        }else{
            $item.FullName
        }
    }
}

$repository = "https://gitlab.com/tip-benchmarking/svelte";
$tipServer = "http://localhost:9119"

$params = @{
    "repository"=$repository;
    "realTPM"=$true;
}

$DirsToExclude=@(
  "public/build",
  ".git",
  "node_modules"
)


$FilenamesToExclude=@(
	"sw.js"
)

$sourceRoot = Get-Location
$destinationRoot = "${env:TEMP}\tip-$(New-Guid)"
$destinationRoot

New-Item $destinationRoot -itemtype directory
robocopy $sourceRoot $destinationRoot /S /XD $DirsToExclude

Set-Location $destinationRoot
foreach($file in GetFiles "./"){
    if(Test-Path $file -PathType Leaf){
        $File = Get-ChildItem $file
        if($File.Name -in $FilenamesToExclude){
            Remove-Item $File
        }
    }
}

"Compressing..."
Get-ChildItem -Path $destinationRoot -Exclude $DirsToExclude | 
  Compress-Archive -DestinationPath "${destinationRoot}.zip" -Force


"Sending..."
$result = Get-ChildItem "${destinationRoot}.zip" | Send-MultiPartForm "${tipServer}/gateway.php" $params
$result
exit $result